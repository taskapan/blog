


Written with [StackEdit](https://stackedit.io/).


#**MIJN BLOG!**


----------


*Maandag 4 september 2017*

We hebben onze eerste project gekregen, met mijn team samen 
moeten wij een spel bedenken en 15 september moeten we onze 
eerste prototype inleveren. 


----------


*Dinsdag 5 september 2017*

Mijn team en ik hebben er over nagedacht en we zijn begonnen met een onderzoek, we hebben onze taken verdeeld en hebben allemaal een thema.
Mijn thema is **Rotterdam Architectuur**. 

>***Design is to design a design to produce a design***


----------

*Woensdag 6 september 2017*

Ik ben begonnen met onderzoeken en ik heb informatie verzamelt over de oude en moderne gebouwen in Rotterdam. Ik heb een moodboard gemaakt over mijn thema doelgroep en ik ga een bouwkunde student er over intervieuwen. 


----------


*Donderdag 7 september 2017*

Vandaag hebben we met ons team over onze doelgroep en thema besproken wat we 
hebben gemaakt. Ik heb een bouwkunde student geintervieuwd en mijn onderzoek 
afgerond. 


----------
*Maandag 11 september 2017*

Tijdens de design challenge les gingen wij brainstormen over onze spel.
Uiteindelijk hebben we een concept bedacht voor onze spel en hebben we de
prototype ervan gemaakt. 

**Het spel:**
 
>1.	Elk team gooit een dobbelsteen, het team dat het hoogste gooit begint.
2.	Teams spelen omstebeurt.
3.	Het team dat begint draait aan het rad.
4.	Het rad geeft aan welke kleur kaart je moet pakken en je voert de desbetreffende opdracht uit.
5.	Je hebt voor elke opdracht 30 seconde de tijd.
6.	Bij een goed voltooide opdracht zet je 1 punt op je scorekaart.
7.	Bij 5 punten krijg je een Rotterdams vlaggetje.
8.	Wanneer je 1 vlaggetje hebt en het rad op “supervraag” komt mag je een supervraagkaart van het bord pakken en deze beantwoorden.
9.	Wanneer het rad op “supervraag” komt en je team heeft nog geen vlaggetje mag je nog een keer draaien.
10.	Wanneer het team de supervraag goed beantwoord mag dat team een hintkaart pakken.
11.	Wanneer een team elke keer een nieuwe hintkaart heeft mogen ze 1x het onderwerp op de secretcard proberen te raden.
12.	Het team dat als eerste zijn onderwerp raad wint het spel!


----------

*Dinsdag 12 september 2017*

We hebben een workshop over onze blog gekregen en hebben we het uitgewerkt. 


----------

*Woensdag 13 september 2017*

Vandaag hebben we onze eerste workshop gehad over Prototyping, we moesten met ons
groepje binnen 10 minuten een prototype maken van wat wij zouden meenemen als wij
op vakantie gaan. 

Wij hebben met ons groepje er voor gekozen om een vakantiehoed te maken
met een spiegeltje zodat je altijd naar je zelf kunt kijken. Als tweede opdracht moesten 
wij iets bedenken om het samenleving gezonder te maken. 


----------
*Donderdag 14 september 2017*

Hoorcollege Design theory

>**Visuele communicatie**
1. Leren kijken 
2. Leren begrijpen/interpreteren
3. Beeld kiezen/maken

>**Visuele geletterdheid**
1. Gestalt (zien) - (wet van...)
2. Semiotiek (begrijpen) -  Iconisch, Indexiale, Symbolisch
3. Retorica (overtuigen) - Ethos, Pathos, Logos, Kairos 

>**Retorische Stijlfiguren**
Regelmatigheden
- Beeldrijm
- Verbo
- Repetito
- Contrast

 >Onregelmatigheden
 - Metafoor
 - Vergelijking
 - Personificatie
 - Hyperbool
 - Oxymoron


----------

*Vrijdag 15 september 2017*

Vandaag heb ik mijn tweede workshop over prototyping gehad.
Ons opdracht was net als de vorige keer over gezondheid, 
maar dit keer moest het een service zijn voor de doelgroep. 

 

